import java.io.Serializable;

/**
 * Created by pacio on 22.03.2017.
 */
public class Brodcast implements Serializable{
    static final long serialVersionUID=1L;
    private String od;
    private String dokogo;
    private String wiadomosc;
    private boolean rozgloszeniowy;
    public Brodcast(String od, String dokogo, String wiadomosc,boolean rozgloszeniowy){
        this.od=od;
        this.dokogo=dokogo;
        this.wiadomosc=wiadomosc;
        this.rozgloszeniowy=rozgloszeniowy;
    }

    public String getOd() {
        return od;
    }

    public void setOd(String od) {
        this.od = od;
    }

    public String getDokogo() {
        return dokogo;
    }

    public void setDokogo(String dokogo) {
        this.dokogo = dokogo;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }

    public boolean isRozgloszeniowy() {
        return rozgloszeniowy;
    }

    public void setRozgloszeniowy(boolean rozgloszeniowy) {
        this.rozgloszeniowy = rozgloszeniowy;
    }
}
