import java.io.*;
import java.net.Socket;

/**
 * Created by pacio on 15.03.2017.
 */
public class AsyncTask implements Runnable{
    private Socket soc;
    private Menadzer m;
    public Test t;
    public AsyncTask(Socket soc, Menadzer m){
        this.soc=soc;
        this.m=m;
    }
    @Override
    public void run() {
        try {
        System.out.println("SERWER>> Klient podlaczony Watek"+Thread.currentThread().getId());
        InputStream in =  soc.getInputStream();

        OutputStream out = soc.getOutputStream();
        //DataInputStream dis = new DataInputStream(in);
        //DataOutputStream dos = new DataOutputStream(out);
        //Tools.dodajObiekt(new Obiekty("dos", dos));
        //Tools.dodajObiekt(new Obiekty("dis", dis));
         BufferedReader read=new BufferedReader(new InputStreamReader(in));
         BufferedWriter write=new BufferedWriter(new OutputStreamWriter(out));
            ObjectOutputStream oos=new ObjectOutputStream(out);
        //    t=new Test(oos);
         ObjectInputStream ois=new ObjectInputStream(in);

         //Tools.dodajObiekt(new Obiekty("in",Thread.currentThread().getId(),in));
         Tools.dodajObiekt(new Obiekty("oos",Thread.currentThread().getId(),oos));
            Tools.dodajObiekt(new Obiekty("ois",Thread.currentThread().getId(),ois));
        System.out.println("Nazywam sie watek: "+Thread.currentThread().getId());
        Tools.dodajObiekt(new Obiekty("bw",Thread.currentThread().getId(),write));
         Tools.dodajObiekt(new Obiekty("br",Thread.currentThread().getId(),read));
         m.wykonaj();

            System.out.println(">> Czekam na teks");
         //   String napis=Tools.czytajAsync((BufferedReader) Tools.znajdzObiekt("br"));
        //    String napis=read.readLine();
        //    System.out.println(">> klient pise: "+napis);



        //dis.close();
       // dos.close();
            write.close();
            oos.close();
            ois.close();
        read.close();
        in.close();
        out.close();
        soc.close();
        System.out.println(">> Polaczenie z klientem zakonczone");
        Tools.czysc();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
