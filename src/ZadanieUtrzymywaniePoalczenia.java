import java.io.*;

/**
 * Created by pacio on 29.03.2017.
 */
public class ZadanieUtrzymywaniePoalczenia implements  Zadanie{
    String nazwa="Utrzymywanie polaczenia Serwer";
    private  int odp=1;
    private Sasiad s=null;
    @Override
    public String nazwa() {
        return nazwa;
    }
    @Override
    public void wykonaj() {
        BufferedReader br = (BufferedReader) Tools.znajdzObiekt("br");
        BufferedWriter bw= (BufferedWriter) Tools.znajdzObiekt("bw");
        ObjectInputStream ois= (ObjectInputStream) Tools.znajdzObiekt("ois");
        Menadzer m=new Menadzer();
        boolean work=true;
        boolean poczatek=true;

        while(work) {
            System.out.println("Dziala utrzymanie dla watku: "+Thread.currentThread().getId());
            System.out.println("port check: ");
            Tools.wyswietl();
//            int lol= (int) Tools.znajdzObiekt("soc");
//            System.out.println("Port: "+lol);

            if(s!=null) {
                System.err.println("Jestem polaczony z: " + s.getNazwa());
            }else{
                System.out.println("Sasiad nieustalny");
            }
            if (poczatek) {
                System.out.println("<< Wysylanie komunikatu 0 do klienta w celu wyciagniecia jego danych");
                try {
                    System.out.println("Przed wyslaniem");
                    bw.write(0);
                    bw.flush();
                    System.out.println("Komunikat wyslano");
                    Sasiad s = (Sasiad) ois.readObject();
                    ObjectOutputStream oo= (ObjectOutputStream) Tools.znajdzObiekt("oos");
                    if(oo==null){
                        System.err.println("TU NULL PANIE!!");
                    }else{
                        System.err.println("NIE NULL!!");
                    }
                    s.oos=oo;
                    System.out.println("Imie: " + s.getNazwa());
                    System.out.println("Port: " + s.getPort());
                    this.s=s;

                    Sasiad.addOos(s);
                    Sasiad.addSa(s);
                    Sasiad.addSluchacz(s,this);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                poczatek = false;
            }
            if (odp == 1) {
                try {
                    System.out.println("Pisze jedynke");
                   // bw.flush();
                    bw.write(1);
                    bw.flush();
                    System.out.println("Przed zadaniem");

                    System.out.println("Zadanie wykonane");
                    Thread.sleep(10000);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else if(odp==2){

            try {
                System.out.println("Pisze dwojke");
              //  bw.flush();
                bw.write(2);
               // bw.flush();
                System.err.println("Wylalem 2 do "+s.getNazwa());
                System.out.println("Przed zadaniem");
                Tools.wykonajZadania();
                System.out.println("Zadanie wykonane");
                odp=1;
                Thread.sleep(5000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        }
    }

    public int getOdp() {
        return odp;
    }

    public void setOdp(int odp,Sasiad s) {
        System.out.println("Sasiad: "+s.getNazwa()+" modyfikuje dop na 2");
        this.s=s;
        this.odp = odp;
    }
}
