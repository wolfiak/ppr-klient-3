import java.util.Scanner;

/**
 * Created by pacio on 23.03.2017.
 */
public class ZadanieGenerowanieWiadomosci implements Zadanie{
    String nazwa="Generowanie wiadomosc";
    private Brodcast brodcast;

    @Override
    public String nazwa() {
        return nazwa;
    }

    @Override
    public void wykonaj() {
        Scanner skaner = new Scanner(System.in);


        //String wiad = skaner.nextLine();

            System.out.print("Do kogo:");
            String dokogo = skaner.nextLine();
            System.out.print("Wiadomosc: ");
            String wiadomosc = skaner.nextLine();
            System.out.print("Rozgloszeniowy: (t/f): ");
            String odp = skaner.nextLine();
            boolean dec;
            System.out.println("Odpowiedz to: "+odp);
            if (odp == "t") {
                dec = true;
            } else {
                dec = false;
            }
            brodcast = new Brodcast(Info.Imie, dokogo, wiadomosc, dec);
            System.out.println(">> Przed wysylaniem obiektu");
          //  Tools.wyslijObiekt(brodcast);
            Tools.wyslijObiekty(brodcast);
            Tools.check=true;

    }

}
