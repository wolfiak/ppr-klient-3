import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 22.03.2017.
 */
public class Sasiad implements Serializable{
    private static List<Sasiad> sa=new ArrayList<Sasiad>();
    private String ip;
    private String nazwa;
    private boolean nadwaca;
    private int port;
    private int threadId;
    public ObjectOutputStream oos;
    public  ZadanieUtrzymywaniePoalczenia zup;
    public Sasiad(String ip,int port, String nazwa) {
        this.ip = ip;
        this.nazwa = nazwa;
        this.port=port;
    }

    public int getThreadId() {
        return threadId;
    }

    public static List<Sasiad> getSa() {
        return sa;
    }

    public static void addSa(Sasiad s) {
        boolean flaga=false;
        if(sa.size()==0){
            sa.add(s);
        }
        for(int n=0;n<sa.size();n++){
            Sasiad sasiad=sa.get(n);
            if(( (sasiad.getPort()==s.getPort()) ) && (sasiad.getNazwa().equals(s.getNazwa()))){
               flaga=true;

            }else{
                System.out.println("Musi byc inny");

            }
        }
        if(!flaga){
            sa.add(s);
        }
    }
    public static void addSluchacz(Sasiad s,ZadanieUtrzymywaniePoalczenia z) {

        for(int n=0;n<sa.size();n++) {
            Sasiad sasiad = sa.get(n);
            if (((sasiad.getPort() == s.getPort())) && (sasiad.getNazwa().equals(s.getNazwa()))) {
                System.err.println("Dodanie sluchacza dla:"+sasiad.getNazwa());
                sa.get(n).zup=z;

            }
        }
    }
    public static void addOos(Sasiad s){
        for(Sasiad i: sa){
            if(s.getPort()==i.getPort() && s.getNazwa().equals(i.getNazwa())){
                if(i.oos==null){
                    System.err.println("OOS DODANY");
                    i.oos=s.oos;
                }
            }
        }
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public boolean isNadwaca() {
        return nadwaca;
    }

    public void setNadwaca(boolean nadwaca) {
        this.nadwaca = nadwaca;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
